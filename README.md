ThinkCreative Search Bundle
===========================

## Introduction

Implements a search page that uses Apache Solr. Includes an eZ Publish filter.

## Installation

Add the bundle to app/AppKernel.php (ezpublish/EzPublishKernel.php)...

    new ThinkCreative\SearchBundle\ThinkCreativeSearchBundle()

## Sample Config Entry

The following is a sample configuration...

    think_creative_search:
        solr:
            host: "ef"
            port: 8986
            core: "theoneweekboutique"

        page_layout_template: "OneWeekBoutiqueSiteBundle::base.html.twig"

        filter: thinkcreative.search.filter.ezpublish

        search:
            - name: default
              query: "meta_class_identifier_ms:article OR meta_class_identifier_ms:blog_post OR meta_class_identifier_ms:news_item OR meta_class_identifier_ms:event meta_class_identifier_ms:folder"
              highlighting: true
              facet_fields:
                  - display_name: "Content Type"
                    identifier: category
                    field: "meta_class_identifier_ms"
              facet_ranges:
                  - display_name: "Date"
                    identifier: date
                    fields:
                      - "attr_publication_date_dt"
                      - "meta_published_dt"
                    start: "*"
                    end: "NOW"
                    gaps:
                      - name: "Less Than A Week"
                        value: "-7DAYS"
                      - name: "Less Than A Month"
                        value: "-1MONTH"
                      - name: "Less Than 3 Months"
                        value: "-3MONTH"
                      - name: "More Than 3 Months"
                        value: "-3MONTH+1MINUTE"
              sort:
                  - name: "Relevance"
                    value: "score desc, attr_publication_date_dt desc, meta_published_dt desc"
                  - name: "Content Type"
                    value: "meta_class_identifier_ms asc, score desc"
                  - name: "Alphabetically"
                    value: "meta_name_t asc"
                  - name: "Publication Date"
                    value: "attr_publication_date_dt desc, meta_published_dt desc, score desc"

**solr:**
Information needed to connect to solr server.

**page_layout_template:**
Base page layout template to use on search page.

**filter:**
Service that filters Solr field values to human readable values as needed.

**search:**
Search parameters and constraints.

**search.query:**
Solr query to be included in all searches. Can be used to exclude unwanted results.

**search.facet_fields:**
Used to create facet searches based on given field's unique values from search results.

**search.facet_ranges:**
Used to create facet searches based on ranges, such as a date range.

**search.sort:**
Used by sort dropdown on search page to provide sorting options for search results.


## Usage

Get the 'thinkcreative.search.search' and run the method 'search' with the following paramters...

- Search Type :: Search configuration to use
- Query String :: Query string for search
- Limit :: Limit results per page
- Offset :: Result offset
- Sort :: Defines how search results is sorted
- Facet Values :: Array of current facet values "facet_identifier => facet_value" ... example: "date => -1MONTH"

Returns ThinkCreative\SearchBundle\Classes\SearchResults.