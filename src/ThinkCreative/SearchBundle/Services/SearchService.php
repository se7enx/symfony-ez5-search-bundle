<?php
namespace ThinkCreative\SearchBundle\Services;
use ThinkCreative\SearchBundle\Solr\SolrQuery;
use ThinkCreative\SearchBundle\Exception\SearchTypeNotAvailableException;
use ThinkCreative\SearchBundle\Classes\SearchResults;
use eZSolr;
use ThinkCreative\SearchBundle\Classes\ezfeZPSolrQueryBuilder;

/**
 * Service for performing SOLR searches
 */
class SearchService
{
    /**
     * Solr configuration
     * @var array
     */
    protected $solrConfig;

    /**
     * Search configuration
     * @var array
     */
    protected $searchConfig;

    /**
     * eZ Publish Legacy Kernel
     * @var ezLegacyKernel
     */
    protected $ezLegacyKernel;

    /**
     * Filter service
     * @var object
     */
    protected $filter;

    protected $logger;

    /**
     * Constructor.
     * @param array $solr_config
     * 
     */
    public function __construct(array $solr_config, array $search_config, \Closure $ez_legacy_kernel, $logger = null)
    {

        // set solr config
        $this->solrConfig = $solr_config;

        // set search config
        $this->setSearchConfig($search_config);

        $this->ezLegacyKernel = $ez_legacy_kernel;

        $this->logger = $logger;
    }

    /**
     * Set search configuration.
     * @param array $search_config
     */
    public function setSearchConfig(array $search_config)
    {
        $this->searchConfig = $search_config;
    }

    /**
     * Set a filter service to use with search 
     * queries
     * @param object $filter_service
     */
    public function setFilter($filter_service)
    {
        $this->filter = $filter_service;
    }

    /**
     * Get array of available sort options
     * @param string $search_type
     */
    public function getSortOptions($search_type = "default")
    { 
        // get search conf for given search type
        $searchConfig = null;
        foreach ($this->searchConfig as $searchConfig) {
            if ($searchConfig['name'] == $search_type) {
                break;
            }
        }
        if (!$searchConfig || $searchConfig['name'] != $search_type) {
            throw new SearchTypeNotAvailableException("Search type '{$search_type}' was requested but not provided in search configuration.");
        }

        return $searchConfig['sort'];
    }

    /**
     * Perform a search.
     * @param string $search_type
     * @param string $query
     * @param integer $limit
     * @param integer $offset
     * @param integer $sort
     * @param array $facet_values
     * @param array $solr_params  Extra parameters for SOLR query
     * @return array
     */
    public function search($search_type = "default", $query, $limit = null, $offset = 0, $sort = "", array $facet_values = array(), $solr_params = array())
    {

        // get search conf for given search type
        $searchConfig = null;
        foreach ($this->searchConfig as $searchConfig) {
            if ($searchConfig['name'] == $search_type) {
                break;
            }
        }
        if (!$searchConfig || $searchConfig['name'] != $search_type) {
            throw new SearchTypeNotAvailableException("Search type '{$search_type}' was requested but not provided in search configuration.");
        }

        // set default sort
        if (!$sort && array_key_exists("sort", $searchConfig)) {
            $sort = $searchConfig['sort'][0]['value'];
        }

        // set default limit
        if ($limit === null) {
            $limit = 10;
            if (array_key_exists("limit", $searchConfig)) {
                $limit = $searchConfig['limit'];
            }
        }

        // init SolrQuery
        $solr = new SolrQuery(
            $this->solrConfig['host'], 
            $this->solrConfig['core'],
            $this->solrConfig['port'],
            $this->solrConfig['protocol'],
            $this->solrConfig['path'],
            $this->filter,
            $this->logger
        );

        // set query
        $solr->query($query);

        // iterate searchConfig and build query
        foreach ($searchConfig as $key => $value) {

            switch($key) {

                // search query
                case "query":
                    $solr->fq($value);
                break;

                // search query
                case "query_handler":
                    $solr->qt($value);
                break;

                // search query
                case "query_fields":
                    if( $value === 'all_searchable' ) {
                        $kernel = $this->ezLegacyKernel;
                        $value  = $kernel()->runCallback(
                            function()
                            {
                                $ezSolr   = new eZSolr();
                                $qBuilder = new ezfeZPSolrQueryBuilder($ezSolr);
                                return $qBuilder->publicGetClassAttributes();
                            }
                        );
                        $value = implode( ' ', $value );
                    }
                    $solr->qf($value);
                break;

                // highlighting
                case "highlighting":
                case "highlight":
                case "hl":
                    $solr->highlighting($value);
                break;
                case "highlight_fields":
                    $solr->highlightingFields($value);
                break;

                // facet field
                case "facet_fields":
                    foreach ($value as $facetFieldConfig) {
                        $facetField = $solr->facetField();
                        foreach ($facetFieldConfig as $ffKey => $ffValue) {
                            switch ($ffKey) {

                                // identifier
                                case "identifier":
                                    $facetField->identifier($ffValue);
                                break;

                                // display name
                                case "display_name":
                                case "displayName":
                                case "displayname":
                                    $facetField->displayName($ffValue);
                                break;

                                // field
                                case "field":
                                    $facetField->field($ffValue);
                                break;

                                // operator
                                case "operator":
                                    $facetField->operator($ffValue);
                                break;

                                // basic facet field parameter
                                default:
                                    if ($facetField->field()) {
                                        $facetField->param("f." . $facetField->field() . ".facet.{$ffKey}", var_export($ffValue, true));
                                    } else {
                                        $facetField->param("facet.{$ffKey}", var_export($ffValue, true));    
                                    }
                                break;
                            }
                        }

                        $facetField->limit(200);
                        // get facet values
                        if ($facet_values && array_key_exists($facetField->identifier, $facet_values)) {
                            $facetValue = $facet_values[$facetField->identifier];
                            if (gettype($facetValue) != "array") {
                                $facetValue = array($facetValue);
                            }
                            $facetField->values($facetValue);
                        }

                    }
                break;

                // facet range
                case "facet_ranges":
                    foreach ($value as $facetRangeConfig) {
                        $facetRange = $solr->facetRange();
                        foreach ($facetRangeConfig as $frKey => $frValue) {
                            switch ($frKey) {

                                // identifier
                                case "identifier":
                                    $facetRange->identifier($frValue);
                                break;

                                // display name
                                case "display_name":
                                case "displayName":
                                case "displayname":
                                    $facetRange->displayName($frValue);
                                break;

                                // fields
                                case "fields":
                                    $facetRange->fields($frValue);
                                break;

                                // start
                                case "start":
                                    $facetRange->start($frValue);
                                break;

                                // end
                                case "end":
                                    $facetRange->end($frValue);
                                break;

                                // gaps
                                case "gaps":
                                case "ranges":
                                    $facetRange->gaps($frValue);
                                break;

                                // default, do nothing
                                default:
                                break;
                            }
                        }

                        // get facet values
                        if ($facet_values && array_key_exists($facetRange->identifier, $facet_values)) {
                            $facetValue = $facet_values[$facetRange->identifier];
                            if (gettype($facetValue) == "array") {
                                $facetValue = $facetValue[0];
                            }
                            $facetRange->value($facetValue);
                        }

                    }
                break;

                // facet queries
                case "facet_queries":
                    foreach ($value as $facetQueryConfig) {
                        $facetQuery = $solr->facetQuery();
                        foreach ($facetQueryConfig as $fqKey => $fqValue) {
                            switch ($fqKey) {

                                // identifier
                                case "identifier":
                                    $facetQuery->identifier($fqValue);
                                break;

                                // display name
                                case "display_name":
                                case "displayName":
                                case "displayname":
                                    $facetQuery->displayName($fqValue);
                                break;

                                // operator
                                case "operator":
                                    $facetQuery->operator($fqValue);
                                break;

                                // queries
                                case "queries":

                                    foreach ($fqValue as $fquery) {

                                        if (gettype($fquery) == "string") {
                                            $facetQuery->addQuery($fquery['query']);
                                        } else {
                                            $facetQuery->addQuery(
                                                $fquery['query'],
                                                isset($fquery['identifier']) ? $fquery['identifier'] : "",
                                                isset($fquery['display_name']) ? $fquery['display_name'] : ""
                                            );
                                        }
                                    }

                                break;

                            }
                        }

                        // get facet values
                        if ($facet_values && array_key_exists($facetQuery->identifier, $facet_values)) {
                            $facetValue = $facet_values[$facetQuery->identifier];
                            if (gettype($facetValue) != "array") {
                                $facetValue = array($facetValue);
                            }
                            $facetQuery->values($facetValue);
                        }
                    }

                break;

                // fl
                case "fl":
                    $solr->param("fl", $value);
                break;

                // facet query operator
                case "facet_query_operator":
                    $solr->setFacetQueryOperator($value);
                break;

                // sort
                case "sort":
                    $solr->sort($sort);
                break;

                // basic parameters
                default:
                    $solr->param($key, $value);
                break;

            }

        }

        // offset
        if ($offset > 0) {
            $solr->offset($offset);
        }

        // limit
        if ($limit > 0) {
            $solr->limit($limit);
        }

        // sort
        if ($sort) {
            $solr->sort($sort);
        }

        // set solr_params
        foreach ($solr_params as $key=>$value) {
            if (!$key || !$value) {
                continue;
            }
            $solr->param($key, $value);
        }

        // perform search
        return new SearchResults(
            array_merge(
                array(
                    "params" => array_merge(
                        $facet_values,
                        array(
                            "query" => $query,
                            "offset" => $offset,
                            "limit" => $limit,
                            "sort" => $sort,
                        )
                    )
                ),
                $solr->search()
            )
        );


    }

}
