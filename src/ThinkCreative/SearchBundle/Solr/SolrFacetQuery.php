<?php

namespace ThinkCreative\SearchBundle\Solr;

class SolrFacetQuery
{

    /**
     * Counter to counter number of instances.
     * @var integer $instanceCount
     */
    protected static $instanceCount = 0;

    /**
     * Parent query object.
     * @var ThinkCreative\SearchBundle\Classes\SolrQuery $parent
     */
    protected $parent;

    /**
     * Array of queries.
     * @var array
     */
    protected $queries;

    /**
     * Array of query fields to use in fq
     * @param array
     */
    protected $values = array();

    /**
     * Operator to use when stringing facets together
     * @param string
     */
    protected $operator = "AND";

    /**
     * Identifier that uniquely identifies this
     * object instances. SPL_OBJECT_HASH used
     * if identifier isn't provided.
     * @var string $identifier
     */
    public $identifier = "";

    /**
     * Name to display to the user.
     * @var string $displayName
     */
    public $displayName = "";

    /**
     * Construct.
     * @param ThinkCreative\SearchBundle\Solr\SolrQuery $parent
     */
    public function __construct(\ThinkCreative\SearchBundle\Solr\SolrQuery $parent)
    {
        $this->parent = $parent;
        $this->identifier = "field_" . (++self::$instanceCount);
        $this->displayName = $this->identifier;
    }

    /**
     * Get/set unique identifier for this object.
     * @param string $value
     * @return SolrFacetField|string
     */
    public function identifier($value = "")
    {
        // set identifier
        if ($value) {
            $this->identifier = preg_replace("/[^A-Za-z0-9_]/", '', $value);
            return $this;
        }

        // get identifier
        return $this->identifier;
    }

    /**
     * Get/set display name for this object.
     * @param string $value
     * @return SolrFacetField|string
     */
    public function displayName($value = "")
    {

        // set
        if ($value) {
            $this->displayName = $value;
            return $this;
        }
        // get
        return $this->displayName;
    }

    /**
     * Add a query to this object
     * @param string $query
     * @param string $indentifer
     * @param string $display_name
     */
    public function addQuery($query, $identifier = "", $display_name = "") 
    {
        $this->queries[] = array(
            "query" => $query,
            "identifier" => $identifier ?: $query,
            "display_name" => $display_name ?: ( $identifier ?: $query ),
        );
    }

    /**
     * Return array of queries.
     * @return array
     */
    public function getQueries()
    {
        return $this->queries;
    }

    /**
     * Array of identifiers to use in filter query (fq)
     *
     */ 
    public function values(array $values) {
        $this->values = $values;
    }

    /**
     * Get/Set operator to use to string facets together
     * @param string $value
     */
    public function operator($value = "")
    {
        if ($value) {
            $this->operator = trim(strtoupper($value));
        } else {
            return $this->operator;
        }
    }

    /**
     * Build URL query string.
     * @return string
     */
    public function build()
    {
        $urlStr = "";
        foreach ($this->getQueries() as $query) {
            if ($urlStr) {
                $urlStr .= "&";
            }
            $urlStr .= "facet.query=" . urlencode($query['query']);
        }
        return $urlStr;
    }

    /**
     * Returns filter query to perform
     * @return string
     */
    public function fq()
    {

        if (!$this->values) {
            return "";
        }

        $urlQuery = "";
        foreach ($this->queries as $query) {
            if (!in_array($query['identifier'], $this->values)) {
                continue;
            }
            if ($urlQuery) {
                $urlQuery .= "%20" . $this->operator . "%20";
            }
            $urlQuery .= "(" . urlencode($query['query']) . ")";
        }

        return trim($urlQuery);
    }

    /**
     * Returns $parent
     * @var ThinkCreative\SearchBundle\Classes\SolrQuery
     */
    public function done()
    {
        return $this->parent;
    }

}